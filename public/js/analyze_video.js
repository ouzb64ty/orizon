$(document).ready(function(){

    /* For ChartJS */
    var canvas = document.getElementById('detectionsStats');
    var token = getCookie("token");

    /* Tools */

    function createCORSReq(method, url) {

        let req = new XMLHttpRequest();

        if ("withCredentials" in req) {
            req.open(method, url, true); 
        } else if (typeof XDomainRequest != "undefined") {
            req = new XDomainRequest();
            req.open(method, url, true); 
        } else {
            req.open(method, url, true); 
        }
        return (req);
    }

    /* Cookies */

    function getCookie(cname) {

        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');

        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];

            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* Analyze */

    var analyzeBtn = document.getElementById("analyzeBtn");
    var id = analyzeBtn.parentElement.id;

    analyzeBtn.addEventListener("click", function(e) {
        var request = createCORSReq('POST', 'http://127.0.0.1:8003/vision/v1/active_videos/');

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200)
                    $("#successModal").modal('show');
                else if (request.status === 449)
                    $("#waitModal").modal('show');
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send("id=" + id);
        e.preventDefault();
    });

    /* Export */

    var exportBtn = document.getElementById("exportBtn");

    exportBtn.addEventListener("click", function(e) {
        var request = createCORSReq('POST', 'http://127.0.0.1:8001/orizon/v1/videos/export/' + id);
        var from = $("#rgSlider").data().from;
        var to = $("#rgSlider").data().to;

        $("#exportModal").modal('show');
        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                const resJSON = JSON.parse(request.responseText);

                window.location.href = resJSON.url;
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send('from=' + from + '&to=' + to);
        e.preventDefault();
    });

    /* Trash buttons */

    var trashVideo = document.getElementById("trash");

    trashVideo.addEventListener("click", function(e) {
        let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/videos/' + id);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                window.location.href = 'http://127.0.0.1:8000/list_video';
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });

    /* Get video time in hh:mm:ss format */

    function getTiming(secunds)
    {
        var h = 0;
        var m = 0;
        var s = 0;

        h = Math.floor(secunds / 3600);
        secunds = secunds - h * 60;
        m = Math.floor(secunds / 60);
        secunds = secunds - m * 60;
        s = secunds;

        h = h.toString();
        if (h.lenght === 1) h = '0' + h;
        m = m.toString();
        if (m.lenght === 1) m = '0' + m;
        s = s.toString();
        if (s.lenght === 1) s = '0' + s;
        return h + ':' + m + ':' + s;
    }

    /* Parse detection API request */

    function getFramesNb(detections) {
        var result = 0;
        var framesNb = [];

        for (var i = 0; i < detections.length; i++) {
            framesNb.push(getTiming(i));
        }
        return (framesNb);
    }    

    function getPersonnes(detections) {
        var result = 0;
        var personnes = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 0) result++;
            }
            personnes.push(result);
            result = 0;
        }
        return (personnes);
    }

    function getVelos(detections) {
        var result = 0;
        var velos = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 1) result++;
            }
            velos.push(result);
            result = 0;
        }
        return (velos);
    }

    function getVoitures(detections) {
        var result = 0;
        var voitures = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 2) result++;
            }
            voitures.push(result);
            result = 0;
        }
        return (voitures);
    }

    function getMotos(detections) {
        var result = 0;
        var motos = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 3) result++;
            }
            motos.push(result);
            result = 0;
        }
        return (motos);
    }

    function getBus(detections) {
        var result = 0;
        var bus = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 4) result++;
            }
            bus.push(result);
            result = 0;
        }
        return (bus);
    }

    function getCamions(detections) {
        var result = 0;
        var camions = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 5) result++;
            }
            camions.push(result);
            result = 0;
        }
        return (camions);
    }

    function getSacsAMain(detections) {
        var result = 0;
        var sacsAMain = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 7) result++;
            }
            sacsAMain.push(result);
            result = 0;
        }
        return (sacsAMain);
    }

    function getSacsADos(detections) {
        var result = 0;
        var sacsADos = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 6) result++;
            }
            sacsADos.push(result);
            result = 0;
        }
        return (sacsADos);
    }

    function getValises(detections) {
        var result = 0;
        var valises = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 8) result++;
            }
            valises.push(result);
            result = 0;
        }
        return (valises);
    }

    function getAnimaux(detections) {
        var result = 0;
        var animaux = [];

        for (var i = 0; i < detections.length; i++) {
            for (var y = 0; y < detections[i].dets.length; y++) {
                if (detections[i].dets[y].class == 9) result++;
            }
            animaux.push(result);
            result = 0;
        }
        return (animaux);
    }

    function classIntToString(classes) {
        if (classes == 0) return ('Personne');
        else if (classes == 1) return ('Velo');
        else if (classes == 2) return ('Voiture');
        else if (classes == 3) return ('Moto');
        else if (classes == 4) return ('Bus');
        else if (classes == 5) return ('Camion');
        else if (classes == 6) return ('Sac a dos');
        else if (classes == 7) return ('Sac a main');
        else if (classes == 8) return ('Valise');
        else if (classes == 9) return ('Animal');
        else return ('Unknown');
    }
    
    function colorIntToString(classes) {
        if (classes == 0) return ('Blanc');
        else if (classes == 1) return ('Noir');
        else if (classes == 2) return ('Rouge');
        else if (classes == 3) return ('Orange');
        else if (classes == 4) return ('Jaune');
        else if (classes == 5) return ('Vert');
        else if (classes == 6) return ('Bleu');
        else if (classes == 7) return ('Violet');
        else if (classes == 8) return ('Magenta');
        else return ('Unknown');
    }

    /* Init chart and array detections */

    var request = createCORSReq('GET', 'http://127.0.0.1:8003/vision/v1/detections/videos/' + id);
    var ctx = canvas.getContext('2d');
    var tbody = document.getElementById('arrayDets');
    var chart;
    var labelI;

    request.onreadystatechange = function () {

        if(request.readyState === 4 && request.status === 200) {
            var responseJSON = JSON.parse(request.response).detections;

            for (var i = 0; i < responseJSON.length; i++) {
                for (var i2 = 0; i2 < responseJSON[i].dets.length; i2++) {
                    var tr = document.createElement('tr');
                    var tdClass = document.createElement('td');
                    var tdColor = document.createElement('td');
                    var tdUrl = document.createElement('td');
                    var tdSec = document.createElement('td');

                    tdSec.innerHTML = '<a target="_blank" href="' + responseJSON[i].src + '">' + getTiming(responseJSON[i].progressSec / 1000) + '</a>';
                    tdClass.innerHTML = classIntToString(responseJSON[i].dets[i2].class);
                    tdColor.innerHTML = colorIntToString(responseJSON[i].dets[i2].color);
                    tr.appendChild(tdSec);
                    tr.appendChild(tdClass);
                    tr.appendChild(tdColor);
                    tbody.appendChild(tr);
                }
            }
            chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: getFramesNb(responseJSON),
                    datasets: [{
                        label: 'Personne',
                        data: getPersonnes(responseJSON),
                        borderColor: [
                            '#f6e58d'
                        ],
                        fill: false
                    },
                        {
                            label: 'Velo',
                            data: getVelos(responseJSON),
                            borderColor: [
                                '#ffbe76'
                            ],
                            fill: false
                        },
                        {
                            label: 'Voiture',
                            data: getVoitures(responseJSON),
                            borderColor: [
                                '#ff7979'
                            ],
                            fill: false
                        },
                        {
                            label: 'Moto',
                            data: getMotos(responseJSON),
                            borderColor: [
                                '#7ed6df'
                            ],
                            fill: false
                        },
                        {
                            label: 'Bus',
                            data: getBus(responseJSON),
                            borderColor: [
                                '#fbc531'
                            ],
                            fill: false
                        },
                        {
                            label: 'Camion',
                            data: getCamions(responseJSON),
                            borderColor: [
                                '#e056fd'
                            ],
                            fill: false
                        },
                        {
                            label: 'Sac à dos',
                            data: getSacsADos(responseJSON),
                            borderColor: [
                                '#34e7e4'
                            ],
                            fill: false
                        },
                        {
                            label: 'Sac à main',
                            data: getSacsAMain(responseJSON),
                            borderColor: [
                                '#ffdd59'
                            ],
                            fill: false
                        },
                        {
                            label: 'Valise',
                            data: getValises(responseJSON),
                            borderColor: [
                                '#686de0'
                            ],
                            fill: false
                        },
                        {
                            label: 'Animal',
                            data: getAnimaux(responseJSON),
                            borderColor: [
                                '#30336b'
                            ],
                            fill: false
                        }]
                },
                options : {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                userCallback: function(label, index, labels) {
                                    // when the floored value is the same as the value we have a whole number
                                    if (Math.floor(label) === label) {
                                        return label;
                                    }
                                },
                            }
                        }]
                    },
                }
            });
            labelI = chart.data.labels.length;
        }
    };
    request.setRequestHeader("Authorization", "Bearer " + token);
    request.send();

    /* WS add detections */

    let wsVision = io.connect('http://127.0.0.1:8002');
    var progressBar = document.getElementById("progressBar");

    wsVision.on('dets', function(data) {
        if (data.assoc == id) {
            for (var i2 = 0; i2 < data.dets.length; i2++) {
                var tr = document.createElement('tr');
                var tdClass = document.createElement('td');
                var tdColor = document.createElement('td');
                var tdUrl = document.createElement('td');
                var tdSec = document.createElement('td');

                tdSec.innerHTML = '<a target="_blank" href="' + data.src + '">' + data.progressSec / 1000 + '</a>';
                tdClass.innerHTML = classIntToString(data.dets[i2].class);
                tdColor.innerHTML = colorIntToString(data.dets[i2].color);
                tr.appendChild(tdSec);
                tr.appendChild(tdClass);
                tr.appendChild(tdColor);
                tbody.appendChild(tr);
            }
            chart.data.labels.push(getTiming(labelI));
            chart.data.datasets[0].data.push(getPersonnes([data]));
            chart.data.datasets[1].data.push(getVelos([data]));
            chart.data.datasets[2].data.push(getVoitures([data]));
            chart.data.datasets[3].data.push(getMotos([data]));
            chart.data.datasets[4].data.push(getBus([data]));
            chart.data.datasets[5].data.push(getCamions([data]));
            chart.data.datasets[6].data.push(getSacsADos([data]));
            chart.data.datasets[7].data.push(getSacsAMain([data]));
            chart.data.datasets[8].data.push(getValises([data]));
            chart.data.datasets[9].data.push(getAnimaux([data]));
            chart.update();
            labelI++;
        }
    });

    /* Edit Video Modal */

    var editVideoBtn = document.getElementById("editVideoBtn");

    editVideoBtn.addEventListener("click", function(e) {
        $("#editModal").modal('show');
        e.preventDefault();
    });

    /* Edit domain */

    var editDomainBtn = document.getElementById("editDomainBtn");

    editDomainBtn.addEventListener("click", function(e) {
        var editDomainInput = document.getElementById("editDomainInput").value;
        var request = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/videos/domain/' + id);

        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                location.reload(true);
            }
        };
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send("domain=" + editDomainInput);
    });

    /* Edit description */

    var editDescriptionBtn = document.getElementById("editDescriptionBtn");

    editDescriptionBtn.addEventListener("click", function(e) {
        var editDescriptionInput = document.getElementById("editDescriptionInput").value;
        var request = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/videos/description/' + id);

        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                location.reload(true);
            }
        };
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send("description=" + editDescriptionInput);
    });

    /* Range Slider */

    var video = document.getElementById('video');
    var width = video.videoWidth;
    var height = video.videoHeight;

    $("#rgSlider").ionRangeSlider({
        skin: "flat",
        type: "double",
        min: 0,
        max: Math.round(video.duration),
        from: 0,
        to: 1,
        postfix: "sec",
        step: 1,
    });

});