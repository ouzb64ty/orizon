$(document).ready(function(){

    /* Requires */

    const token = getCookie("token");
    const username = document.getElementById('usernameTd').innerText;

    /* Tools */

    function createCORSReq(method, url) {

        let req = new XMLHttpRequest();

        if ("withCredentials" in req) {
            req.open(method, url, true); 
        } else if (typeof XDomainRequest != "undefined") {
            req = new XDomainRequest();
            req.open(method, url, true); 
        } else {
            req.open(method, url, true); 
        }
        return (req);
    }

    /* Cookies */

    function getCookie(cname) {

        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');

        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];

            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* Update username */

    var usernameBtn = document.getElementById('usernameBtn');
    var usernameInput = document.getElementById('usernameInput');

    usernameBtn.addEventListener('click', function(e) {
    	let req = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/auth/username/' + usernameInput.value);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                window.location.replace("http://127.0.0.1:8000/login/reconnection");
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });

    /* Update mail */

    var mailBtn = document.getElementById('mailBtn');
    var mailInput = document.getElementById('mailInput');
    var mailConfInput = document.getElementById('mailConfInput');

    mailBtn.addEventListener('click', function(e) {
    	if (mailInput.value == mailConfInput.value) {
    		let req = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/auth/mail/' + mailInput.value);

    	    req.onreadystatechange = function () {
    	        if(req.readyState === 4 && req.status === 200) {
    	            window.location.replace("http://127.0.0.1:8000/login/reconnection");
    	        }
    	    };
    	    req.setRequestHeader("Authorization", "Bearer " + token);
    	    req.send();
    	}
    });

    /* Update password */

    var passwordBtn = document.getElementById('passwordBtn');
    var passwordInput = document.getElementById('passwordInput');
    var lastPasswordInput = document.getElementById('lastPasswordInput');
    var confPasswordInput = document.getElementById('confPasswordInput');

    passwordBtn.addEventListener('click', function(e) {
        var formData = new FormData();

    	if (passwordInput.value == confPasswordInput.value) {
    		let req = createCORSReq('PUT', 'http://127.0.0.1:8001/orizon/v1/auth/password/' + passwordInput.value);

    	    req.onreadystatechange = function () {
    	        if(req.readyState === 4 && req.status === 200) {
    	            window.location.replace("http://127.0.0.1:8000/login/reconnection");
    	        }
    	    };
            formData.set("lastPassword", lastPasswordInput.value);
    	    req.setRequestHeader("Authorization", "Bearer " + token);
    	    req.send(formData);
    	}
    });

    /* Delete account */

    var deleteAccountBtn = document.getElementById('deleteAccountBtn');

    deleteAccountBtn.addEventListener('click', (e) => {
        $("#confirmDeleteModal").modal('show');
    });

    var confirmDeleteBtn = document.getElementById('confirmDeleteBtn');

    confirmDeleteBtn.addEventListener('click', (e) => {
        let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/auth/user/' + username);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                window.location.replace("http://127.0.0.1:8000/disconnection");
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });

    /* Generate pass input */

    var generatePassBtn = document.getElementById('generatePassBtn');
    var generatePassInput = document.getElementById('generatePassInput');

    function generatePassword() {
        var genPass;
        var nbNb;

        while (7) {
            genPass = Math.random().toString(36).slice(-9);
            nbNb = 0;
            for (let i = 0; i < genPass.length; i++) {
                if ('0123456789'.indexOf(genPass[i]) !== -1) {
                    nbNb++;
                } else if (nbNb == 3) {
                    return (genPass);
                }
            }
        }
    }

    generatePassBtn.addEventListener('click', (e) => {
        generatePassInput.value = generatePassword();
        e.preventDefault();
    });

});