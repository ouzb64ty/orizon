var canvas = document.getElementById("boxAnnotation");
var ctx;
var firstX = 0;
var firstY = 0;
var pointer = false;
var img = new Image();
var areas = [];
var video = document.getElementById('video');
const token = getCookie("token");
var videoAreaInfo = document.getElementById('videoAreaInfo');
var id = videoAreaInfo.parentElement.id;

/* Tools */

function getMousePos(canvas, e) {
    var rect = canvas.getBoundingClientRect();

    return {
      	x: e.clientX - rect.left,
      	y: e.clientY - rect.top
    };
}

function refresh() {
	ctx.font = "15px Arial";
	ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
	for (var i = 0; i < areas.length; i++)
		drawArea(areas[i]);
}

/* Deleting area */

function removeArea(area) {
    let req = createCORSReq('DELETE', 'http://127.0.0.1:8002/vision/v1/areas/' + id);
    let body = {};

    body.name = area.name;
    body.x = area.coords[0];
    body.y = area.coords[1];
    body.w = area.coords[2];
    body.h = area.coords[3];
    req.setRequestHeader("Content-Type", "application/json");
    req.setRequestHeader("Authorization", "Bearer " + token);
    req.send(JSON.stringify(body));
}

function checkDel(x, y) {
    for (var i = 0; i < areas.length; i++) {
		area = areas[i];
		if ((x < area.coords[0] + area.coords[2]
			&& x > area.coords[0] + area.coords[2] - 10)
			&& (y < area.coords[1] && y > area.coords[1] - 10))
		{
			var areaToRemove = document.getElementById(areas[i].name + "List");

            removeArea(area);
			areas.splice(i, 1);
			areaToRemove.parentNode.removeChild(areaToRemove);
		    return (true);
		}
    }
    return (false);
}

function drawArea(area) {
	var topCoords = [];

	if (area.coords[2] < 0) {
		topCoords.push(area.coords[0] + area.coords[2]);
		topCoords.push(area.coords[0]);
	} else {
		topCoords.push(area.coords[0]);
		topCoords.push(area.coords[0] + area.coords[2]);
	}
	if (area.coords[3] < 0) topCoords.push(area.coords[1] + area.coords[3]);
	else topCoords.push(area.coords[1]);
	if (area.name) {
	    ctx.strokeText(area.name, topCoords[0], topCoords[2]);
	    ctx.strokeText('x', topCoords[1] - 10, topCoords[2]);
	}
	ctx.strokeRect(area.coords[0], area.coords[1], area.coords[2], area.coords[3]);
}

/* Prevent negative values for width and height */

function coordsRectify(x, y, w, h)
{
    if (w < 0)
    {
		x = x + w;
		w = w * -1;
    }
    if (h < 0)
	{
		y = parseInt(y + h);
		h = h * -1;
    }
    coords = [x, y, w, h];
    return (coords);
}

function pushArea(area) {
    let req = createCORSReq('POST', 'http://127.0.0.1:8002/vision/v1/areas');

    req.setRequestHeader("Content-Type", "application/json");
    req.setRequestHeader("Authorization", "Bearer " + token);
    req.send(JSON.stringify(area));
}

function addArea(markName, x, y, w, h, boolean) {
    var area = {};
    var li = document.createElement('li');
    var iTrash = document.createElement('i');
    var listAnnotation = document.getElementById('listAnnotation');
    
    area.name = markName;
    area.assoc = id;
    area.coords = coordsRectify(x, y, w, h);
    drawArea(area);
    li.innerHTML = area.name;
    li.id = area.name + "List";
    li.className = "list-group-item d-flex justify-content-between align-items-center";
    iTrash.className = 'fas fa fa-fw fa-trash';
    iTrash.style.float = 'right';
    iTrash.style.cursor = 'pointer';
    iTrash.onclick = function() {
    	for (var i = 0; i < areas.length; i++) {
    		if (areas[i].name == area.name
                && areas[i].coords[0] == area.coords[0]
                && areas[i].coords[1] == area.coords[1]
                && areas[i].coords[2] == area.coords[2]
                && areas[i].coords[3] == area.coords[3]) {
                removeArea(area);
    			areas.splice(i, 1);
    			refresh();
    		}
    	}
    	li.parentNode.removeChild(li);
    }
    li.appendChild(iTrash);
    listAnnotation.appendChild(li);
    areas.push(area);
    if (boolean == true) pushArea(area);
}

/* On Mouse Move */

canvas.addEventListener('mousemove', function(e) {
	var pos = getMousePos(canvas, e);
	var tmpArea = {};

	if (pointer == true) {
		tmpArea.coords = [firstX, firstY, pos.x - firstX, pos.y - firstY];
		refresh();
		drawArea(tmpArea);
	}
});

/* On Mouse Down */

canvas.addEventListener('mousedown', function(e) {
	var pos = getMousePos(canvas, e);

	firstX = pos.x;
	firstY = pos.y;
	pointer = true;
});

/* On Mouse Up */

canvas.addEventListener('mouseup', function(e) {
	var pos = getMousePos(canvas, e);
	var addModalValidateBtn = document.getElementById('addModalValidateBtn');
	var markName = document.getElementById('markName');

    pointer = false;
    if (checkDel(pos.x, pos.y) == false)
    {
		ctx.fillStyle = "#000000";
		$("#addModal").modal('show');
		addModalValidateBtn.onclick = function() {
			if (markName.value)
		    	addArea(markName.value, firstX, firstY, pos.x - firstX, pos.y - firstY, true);
		};
    }
    refresh();
});

/* On Load */

function draw(video) { 
    var video = document.getElementById('video');
    ctx = canvas.getContext('2d');

    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);  
    canvas.toBlob = (blob) => {
    	const img = new Image();

    	refresh();
    	img.src = window.URL.createObjectUrl(blob);
    };
}

/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true); 
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true); 
    } else {
        req.open(method, url, true); 
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');

    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var body = {};

/* Load from drawing box annotations */
let req = createCORSReq('GET', 'http://127.0.0.1:8002/vision/v1/areas/' + id);

req.onreadystatechange = function () {
    if(req.readyState === 4 && req.status === 200) {
        var jsonResponse = JSON.parse(req.responseText);

        for (var i = 0; i < jsonResponse.areas.length; i++) {
            addArea(
                jsonResponse.areas[i].name,
                jsonResponse.areas[i].coords[0],
                jsonResponse.areas[i].coords[1],
                jsonResponse.areas[i].coords[2],
                jsonResponse.areas[i].coords[3],
                false);
        }
    }
};

req.setRequestHeader("Authorization", "Bearer " + token);
req.send();
