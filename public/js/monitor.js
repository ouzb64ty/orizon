let visionModule = io.connect('http://127.0.0.1:8002');

visionModule.on('dets', function (data) {
    console.dir(data);
    const tbody = document.getElementById('monitor-body');
    const tr = document.createElement('tr');
    const a_filename = document.createElement('a');

    const th_filename = document.createElement('th');
    const th_res = document.createElement('th');
    const th_nbDets = document.createElement('th');

    a_filename.href = data.filename.substr(10);
    // Here
    a_filename.innerHTML = data.join;
    a_filename.setAttribute("target", "_blank");

    th_filename.scopeName = "col";

    th_res.scopeName = "col";
    th_res.textContent = data.res;

    th_nbDets.scopeName = "col";
    th_nbDets.textContent = data.dets.length;

    monitor.appendChild(tbody);
    tbody.appendChild(tr);
    tr.appendChild(th_filename);
    tr.appendChild(th_res);
    tr.appendChild(th_nbDets);
    th_filename.appendChild(a_filename);
});
