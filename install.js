const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/orizon';

(async function() {
    try {

    	const client = await MongoClient.connect(uri,{ useNewUrlParser: true });
		const bcrypt = require('bcrypt');
	    const datetime = require('node-datetime');
	    const user = process.argv[1];
	    const password = process.argv[2];
	    const mail = process.argv[3];
	    const db = await client.db();

		/* Insert admin user */
	    bcrypt.hash(password, 10, (err, hash) => {

	        if (err) {

	        	console.log("[USER] Erreur lors de l'initialisation du mot de passe, veuillez réessayer..");
	        	process.exit(0);

	        } else {

		        const dt = datetime.create();
		        const formatted = dt.format('m/d/Y H:M:S');

		        db.collection('users').insertOne({
		            'username': user,
		            'password': hash,
		            'mail': mail,
		            'datetime': formatted,
		            'verified': true
		        }, (err, dbResult) => {

		            if (err) throw err;

		            console.log("[USER] Compte administrateur généré");
		            process.exit(0);

		        });
		    }

		});

	} catch(e) {

        /* DB error connection */
        console.error(e);
        
    }
    
})()