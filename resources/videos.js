const ObjectID = require('mongodb').ObjectID;

module.exports = class Videos {

    static updateDomain(db, req, res, user) {
        var ObjectID = require('mongodb').ObjectID;
        var fs = require('fs');

        db.collection("videos").findOne({ '_id': ObjectID(req.params.id) }, (err, video) => {

            var oldPath = 'public/videos/' + video.namespace;
            var newPath = 'public/videos/' + req.body.domain;

            fs.rename(oldPath, newPath, function (err) {
                if (err) throw err;

                db.collection("videos").updateOne({ '_id': ObjectID(req.params.id) }, {$set: {"namespace": req.body.domain}}, (err) => {
                    if (err) throw err;

                    res.json({'success': 'Domain correctly update'});
                });
            });
        });
    }

    static updateDescription(db, req, res, user) {
        var ObjectID = require('mongodb').ObjectID;

        db.collection("videos").updateOne({ '_id': ObjectID(req.params.id) }, {$set: {"description": req.body.description}}, (err) => {
            if (err) throw err;
        });
        res.json({'success': 'Description correctly update'});
    }

    /**
     * @api {post} /orizon/v1/videos Add new video
     * @apiName addVideo
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {Object} namespace Videos parent directory.
     * @apiParam {Object} path Video(s) to upload (in body).
     *
     * @apiSuccess {Object} success Videos are uploads
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Videos are uploads'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Internal Server Error
     *     {
     *          'error': 'Invalid params'
     *     }
     */

    static addVideo(db, req, res, user) {

        const path = require('path');
        const mv = require('mv');
        const date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();

        new Promise((resolve, reject) => {
            let videos = [];

            if (req.files) {
                let keys = Object.keys(req.files);

                for (let key in req.files) {
                    let index = keys.indexOf(key);
                    let nextFileName = keys[index + 1];

                    if (typeof nextFileName !== undefined) {
                        req.files[key].namespace = req.body.namespace;
                        req.files[key].description = req.body.description;
                        req.files[key].username = user.payload.username;
                        req.files[key].tags = req.body.tags.split(',');
                        req.files[key].date = 'le ' + date + '/' + month + '/' + year + ' à ' + hours + ':' + minutes + ':' + seconds;
                        delete req.files[key].data;
                        videos.push(req.files[key]);
                    }
                }
                resolve(videos);
            } else {
                reject();
            }
        }).then((videos) => {

            /* Move and save videos */
            db.collection('videos').insertMany(videos, (err, dbResult) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    videos.forEach((video) => {
                        const uploadPath = 'public/videos/' + video.namespace + '/' + video.name;

                        mv(video.tempFilePath, uploadPath, {mkdirp: true}, (err) => {
                            if (err) res.json({ 'error': 'when moving video' });
                            else res.json({ 'success' : 'Videos are uploads' });
                        });

                    });

                }

            });

        }).catch(() => {

            res.statusCode = 400;
            res.json( { 'error' : 'Invalid params' } );

        });

    }

    /**
     */

    static exportVideo(db, req, res, user) {

        const ObjectID = require('mongodb').ObjectID;
        var fs = require('fs');
        var spawn = require("child_process").spawn;

        db.collection('videos').findOne({ '_id': ObjectID(req.params.id) },
            (err, video) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    if (!fs.existsSync('public/exports'))
                        fs.mkdir('public/exports', { recursive: true }, (err) => { if (err) throw err; });
                    if (!fs.existsSync('public/exports/' + video.namespace))
                        fs.mkdir('public/exports/' + video.namespace, { recursive: true }, (err) => { if (err) throw err; });

                    var urlVideo = encodeURI('http://127.0.0.1:8000/videos/' + video.namespace + '/' + video.name);
                    var exportPath = 'public/exports/' + video.namespace + '/' + video.name;
                    var ffmpeg = spawn('ffmpeg', ['-y', '-i', urlVideo, '-ss', req.body.from, '-t', parseInt(req.body.to) - parseInt(req.body.from), exportPath]);

                    ffmpeg.on('close', (code) => {
                        res.json({
                            'url': encodeURI('http://127.0.0.1:8000/exports/' + video.namespace + '/' + video.name)
                        });
                    });

                }

            });
    }

    /**
     * @api {get} /orizon/v1/videos Get all videos
     * @apiName getVideos
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} videos All uploaded videos
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "_id": "5cab4ba22a645f864ed97066",
     *          "name": "Screencast from 18-06-2018 11_44_41.webm",
     *          "size": 2088052,
     *          "encoding": "7bit",
     *          "tempFilePath": "/tmp/tmp-1-1554729890178",
     *          "truncated": false,
     *          "mimetype": "video/webm",
     *          "md5": "93d774a9afa345b775240010caac8e32",
     *          "path": "uploads/Screencast from 18-06-2018 11_44_41.webm",
     *          "namespace": "yolo"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getVideos(db, req, res, user) {

        if (user.payload.is_admin == true) {

            db.collection('videos').find({}).toArray(
            (err, videos) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({videos});

                }

            });

        } else {

            db.collection('videos').find({'username': user.payload.username}).toArray(
            (err, videos) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({videos});

                }

            });

        }

    }

    /**
     * @api {get} /orizon/v1/videos/username/{username} Get all videos by username
     * @apiName getVideos
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} videos All uploaded videos by username
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "_id": "5cab4ba22a645f864ed97066",
     *          "name": "Screencast from 18-06-2018 11_44_41.webm",
     *          "size": 2088052,
     *          "encoding": "7bit",
     *          "tempFilePath": "/tmp/tmp-1-1554729890178",
     *          "truncated": false,
     *          "mimetype": "video/webm",
     *          "md5": "93d774a9afa345b775240010caac8e32",
     *          "path": "uploads/Screencast from 18-06-2018 11_44_41.webm",
     *          "namespace": "yolo"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getUserVideos(db, req, res, user) {

        db.collection('videos').find({'username': req.params.username}).toArray(
            (err, videos) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({videos});

                }

            });

    }

    /**
     * @api {get} /orizon/v1/videos/page/{n} Get the N last page videos
     * @apiName getNVideos
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {Number} n N Page (begin at 1) Videos number to give.
     *
     * @apiSuccess {Object} videos N page last videos
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "cameras": [{
     *              "_id": "5c917da654eb9e77915e23e2",
     *              "geojson": {
     *              "type": "Feature",
     *              "geometry": {
     *                  "type": "Point",
     *                  "coordinates": [
     *                      9.1979,
     *                      45.5888
     *                  ]
     *              },
     *              "properties": {
     *                  "url": "http://93.241.11.120:4567/cgi-bin/faststream.jpg",
     *                  "label": "Plage",
     *                  "color": "#000000",
     *                  "description": "",
     *                  "country": "IT",
     *                  "city": "Nova Milanese",
     *                  "zip": null,
     *                  "resgion": null,
     *                  "range": [
     *                      1406353408,
     *                      1406355455
     *                  ]
     *              }
     *          }]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 409 Conflict
     *     {
     *          'error': "Page not found"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getNVideos(db, req, res, user) {

        db.collection('videos').countDocuments().then((count) => {

            let page = 6 * (req.params.n);

            if (page > count) {

                res.statusCode = 409;
                res.json({
                    'error': 'Page not found'
                });

            } else {

                if (user.payload.is_admin == true) {

                    db.collection('videos').find({}).skip(page).limit(6).toArray(
                        (err, videos) => {

                            if (err) {

                                res.statusCode = 500;
                                res.json({
                                    'error': 'Internal Server Error'
                                });
                                throw err;

                            } else {

                                res.json({videos});

                            }

                        });

                } else {

                    db.collection('videos').find({'username': user.payload.username}).skip(page).limit(6).toArray(
                        (err, videos) => {

                            if (err) {

                                res.statusCode = 500;
                                res.json({
                                    'error': 'Internal Server Error'
                                });
                                throw err;

                            } else {

                                res.json({videos});

                            }

                        });

                }

            }

        });

    }

    /**
     * @api {post} /orizon/v1/videos/namespaces Get all namespaces video
     * @apiName getNamespacesVideo
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiSuccess {Object} success Namespaces video
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          ['domain1', 'domain2']
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Internal Server Error
     *     {
     *          'error': 'Invalid params'
     *     }
     */

    static getNamespacesVideo(db, req, res, user) {

        db.collection('videos').find({}).project({'namespace': 1, '_id': 0}).toArray(
            (err, namespaces) => {

                let valueArray = [];

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    let unique = {};

                    /* Filtering namespaces */
                    for (var i = 0; i < namespaces.length; i++) {
                        valueArray.push(namespaces[i].namespace);
                    }

                    /* Remove Dups */
                    valueArray.forEach(function(i) {
                        if(!unique[i]) {
                            unique[i] = true;
                        }
                    });
                    valueArray = Object.keys(unique);
                    res.json({'namespaces': valueArray});

                }

            });

    }

    /**
     * @api {get} /orizon/v1/videos/id/{id} Get one videos
     * @apiName getVideo
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {String} id Camera ID.
     *
     * @apiSuccess {Object} video One uploaded video
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "uploads": [
     *              {
     *                  "_id": "5cab4ba22a645f864ed97066",
     *                  "name": "Screencast from 18-06-2018 11_44_41.webm",
     *                  "size": 2088052,
     *                  "encoding": "7bit",
     *                  "tempFilePath": "/tmp/tmp-1-1554729890178",
     *                  "truncated": false,
     *                  "mimetype": "video/webm",
     *                  "md5": "93d774a9afa345b775240010caac8e32",
     *                  "path": "uploads/Screencast from 18-06-2018 11_44_41.webm",
     *                  "namespace": "yolo"
     *              }
     *          ]
     *      }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getVideo(db, req, res, user) {

        const ObjectID = require('mongodb').ObjectID;

        db.collection('videos').findOne({ '_id': ObjectID(req.params.id) },
            (err, video) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json(video);

                }

            });

    }

    /**
     * @api {delete} /orizon/v1/videos/:id Delete id video with associate datas from vision service
     * @apiName deleteVideos
     * @apiGroup videos
     *
     * @apiHeader {String} token Bearer Token authorization.
     *
     * @apiParam {String} id Camera ID.
     *
     * @apiSuccess {Object} success All video are deleted
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Video are delete'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *          'error': "Need API token, not privileges"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static deleteVideo(db, req, res, user) {

        const ObjectID = require('mongodb').ObjectID;
        const request = require('request');
        const fs = require('fs');

        var deleteFolderRecursive = function(path) {
            if( fs.existsSync(path) ) {
                fs.readdirSync(path).forEach(function(file,index){
                    var curPath = path + "/" + file;
                    if(fs.lstatSync(curPath).isDirectory()) { // recurse
                        deleteFolderRecursive(curPath);
                    } else { // delete file
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        };

        db.collection('videos').findOne({'_id': ObjectID(req.params.id)}, (err, video) => {

            deleteFolderRecursive('public/exports/' + video.namespace);
            deleteFolderRecursive('public/videos/' + video.namespace);

            db.collection('videos').deleteOne(
                { '_id': new ObjectID(req.params.id) },
                (err, dbResult) => {
                    if (err) throw err;

                    request.delete({

                        url: 'http://127.0.0.1:8003/vision/v1/assoc/' + req.params.id,
                        auth: { 'bearer': req.token }

                    }, function(err, httpResponse, body) {

                        if (err) throw err;

                        res.json({'success': 'Video are delete'});
                    });
                });
        });

    }

        /**
     *
     *
     *
     */

    static updateDescription(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) request = { '_id': new ObjectID(req.params.id) };
        else request = { 'username' : user.payload.username, '_id': new ObjectID(req.params.id) };

        db.collection('videos').updateOne(request, {$set: {"description" : req.body.description}});
        res.statusCode = 200;
        res.json({
            'success': 'Description correctly update'
        });
    }


    /**
     *
     *
     *
     */

    static updateDomain(db, req, res, user) {

        var request = {};

        if (user.payload.is_admin == true) request = { '_id': new ObjectID(req.params.id) };
        else request = { 'username' : user.payload.username, '_id': new ObjectID(req.params.id) };

        db.collection('videos').updateOne(request, {$set: {"namespace" : req.body.domain}});
        res.statusCode = 200;
        res.json({
            'success': 'Domain correctly update'
        });
    }


};
